// We need to import the fs module from node to write to a file
import * as fs from 'node:fs'

// This is the function that's being called when the URL goes through
export default defineEventHandler(async (event) => {
  // Read the sent data
  const body = await readBody(event)

  // Filter out the email address
  const email = body['email']

  // Use Node.js to write a file and insert the latest email address
  fs.writeFileSync('./registered-emails.txt', email)

  // Return info back to the front-end and say if everything in here worked
  // (so that we can display success or error messages or whatever)
  return {
    email:  email,
    status: '200',
    error:  false
  }
})